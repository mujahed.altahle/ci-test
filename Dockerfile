FROM ubuntu:latest

RUN apt-get -y update

ADD hello /tmp/hello

CMD [ "/tmp/hello" ]

